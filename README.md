Superflux Utils
=============
Superflux utilities reposotpry


Features
--------
* Mailer - Mail sending library
* S3 - AWS S3 library



Contributors
------------
Ademu Anthony <anthony_ademu@superfluxnigeria.com>


Requirements
------------
* [Composer](https://getcomposer.org/doc/00-intro.md#using-composer)



Installation
------------
modify your composer.json

```json
    "require": {
        ...
        "vendor/superflux": "master"
        ...
    },
    "repositories": [
         ...
        {
            "type": "vcs",
            "url":  " git@bitbucket.org:superfluxteam/superflus.git"
        },
        ...
    ]
```

run `composer update`











